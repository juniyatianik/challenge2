package process;
import java.io.*;
import java.util.*;

import java.util.stream.Collectors;


public class ProcessClass {

    protected static Long findLower(List<Integer> list, int target) {
        return list.stream()
                .filter(i -> i < target)
                .collect(Collectors.counting());
    }

    protected static Long findHigher(List<Integer> list, int target) {
        return list.stream()
                .filter(i -> i > target)
                .collect(Collectors.counting());
    }

    protected static Long groupHigher(String csvFile, int num) {

        try {
            File file = new File(csvFile);
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line = "";
            String[] tempArr;
            List<Integer> list = new ArrayList<>();
            Long result = 0L;
            while((line = bufferedReader.readLine()) != null) {
                tempArr = line.split(";");

                for(int i = 1; i < tempArr.length; ++i) {
                    list.add(Integer.parseInt(tempArr[i]));
                    result=findHigher(list, num);
                }
                System.out.println();
            }

            bufferedReader.close();
            return result;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    protected static Long groupLower(String csvFile, int num) {

        try {
            File file = new File(csvFile);
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line = "";
            String[] tempArr;
            List<Integer> list = new ArrayList<>();
            Long result = 0L;
            while((line = bufferedReader.readLine()) != null) {
                tempArr = line.split(";");

                for(int i = 1; i < tempArr.length; ++i) {
                    list.add(Integer.parseInt(tempArr[i]));
                     result=findLower(list, num);
                }
                System.out.println();
            }

            bufferedReader.close();
            return result;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static double generateMean(String csvFile) {
        try {
            File file = new File(csvFile);
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line = "";
            String[] tempArr;

            int total = 0;
            int total_length = 0;
            double mean = 0;
            while((line = bufferedReader.readLine()) != null) {

                tempArr = line.split(";");

                for(int i = 1; i < tempArr.length; ++i) {
                       total += Integer.parseInt(tempArr[i]);
                       total_length += 1;
                }

                System.out.println();
            }

            mean = total / total_length;

            bufferedReader.close();
            return mean;
        }catch(IOException e) {
            e.printStackTrace();
        }
        return 0;
    }

//    method sort

    public static void processMedian(String csvFile) {
        try {
            File file = new File(csvFile);
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line = "";
            String[] tempArr;

            int total = 0;
            int total_length = 0;
            double median = 0;
            while((line = bufferedReader.readLine()) != null) {

                tempArr = line.split(";");

                for(int i = 1; i < tempArr.length; ++i) {
                       total += Integer.parseInt(tempArr[i]);
                       total_length += 1;
                }

                System.out.println();
            }

            median = total / total_length;

            bufferedReader.close();
        }catch(IOException e) {
            e.printStackTrace();
        }
    }
    public static int generateMedian(){
        int[] arr = {34,67,23,89,15,87};
//        sort(arr);
        Arrays.sort(arr);
        int median = 0;
        int temp = 0;
        for(int i = 0; i < arr.length; i++){
            for(int j = i+1; j < arr.length; j++){
                if(arr[i] > arr[j]){
                    temp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = temp;
                }
            }
        }
        if(arr.length % 2 == 0){
            median = (arr[arr.length/2] + arr[arr.length/2 - 1])/2;
        }else{
            median = arr[arr.length/2];
        }
        return median;
    }
    protected static int generateModus(String csvFile) {
        try {
            File file = new File(csvFile);
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            String line = "";
            List<String> tempArr;

            int maxCount = 0;
            int max = 0;
            while((line = bufferedReader.readLine()) != null) {
                tempArr = Arrays.asList(line.split(";"));

                for(int i = 1; i < tempArr.size(); ++i) {
                    int count = 0;
                    for(int j = 1; j < tempArr.size(); ++j) {
                        if(tempArr.get(i).equals(tempArr.get(j))) {
                            count += 1;
                        }
                    }
                    if(count > maxCount) {
                        maxCount = count;
                        max = Integer.parseInt(tempArr.get(i));
                    }
                }

                System.out.println();
            }

            bufferedReader.close();
            return max;
        }catch(IOException e) {
            e.printStackTrace();
        }
        return 0;
    }

//    make method sorting list
    public static void sort(List<List<Integer>> list) {
//        for(int i = 0; i < list.size(); i++) {
//            for(int j = 0; j < list.size(); j++) {
//                if(list.get(i) > list.get(j)) {
//                    int temp = list.get(i);
//                    list.set(i, list.get(j));
//                    list.set(j, temp);
//                }
//            }
//        }

    }

}
