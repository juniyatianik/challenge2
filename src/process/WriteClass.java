package process;

import java.io.File;
import java.io.IOException;
import java.io.*;

public class WriteClass extends ProcessClass {
    private static String text = "E:\\challenge2\\src\\temp\\text.txt";
    private static String csvFile = "E:\\challenge2\\src\\temp\\data_sekolah.csv";
    public static void writeModus() {
        try {
            File file = new File(text);
            if(!file.exists()) {
                file.createNewFile();
            }
            FileWriter fileWriter = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.write("nilai modus nilai siswa: ");
            bufferedWriter.newLine();
            bufferedWriter.write(Integer.toString(ProcessClass.generateModus(csvFile)));
            bufferedWriter.newLine();
            bufferedWriter.flush();
            bufferedWriter.close();
            System.out.println("sukses ditulis");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void writeGroup(int input) {
        try {
            File file = new File(text);
            if(!file.exists()) {
                file.createNewFile();
            }
            FileWriter fileWriter = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.write("nilai yang lebih besar dari "+input);
            bufferedWriter.newLine();
            bufferedWriter.write(Integer.toString(Math.toIntExact(ProcessClass.groupHigher(csvFile, input))));
            bufferedWriter.newLine();
            bufferedWriter.write("nilai yang lebih kecil dari "+input);
            bufferedWriter.newLine();
            bufferedWriter.write(Integer.toString(Math.toIntExact(ProcessClass.groupLower(csvFile, input))));
            bufferedWriter.flush();
            bufferedWriter.close();
            System.out.println("sukses ditulis");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void writeMean() {
        try {
            File file = new File(text);
            if(!file.exists()) {
                file.createNewFile();
            }
            FileWriter fileWriter = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.write("nilai mean dari data siswa:");
            bufferedWriter.newLine();
            bufferedWriter.write(Double.toString(ProcessClass.generateMean(csvFile)));
            bufferedWriter.newLine();
            bufferedWriter.flush();
            bufferedWriter.close();
            System.out.println("sukses ditulis");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
