package menu;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class MainMenu {

    private static String top = "----------------------------------------------------- ";
    private static String title = "Aplikasi Pengolahan Nilai Siswa";
    public static List<String> mainMenu() {
        List<String> list = new ArrayList<String>();
        list.add(top);
        list.add(title);
        list.add(top);
        list.add("Letakkan file csv yang akan di proses");
        list.add("\n");
        list.add("Pilih menu");
        list.add("1. Generate txt untuk menampilkan modus");
        list.add("2. Generate txt untuk menampilkan rata-rata, median");
        list.add("3. Generate txt untuk grouping data");
        list.add("0. Keluar");
        return list;
    }

    public static List<String> result() {
        List<String> list = new ArrayList<String>();
        list.add(top);
        list.add(title);
        list.add(top);
        list.add("File telah digenerate ");
        list.add("silakan cek");
        list.add("Pilih menu");
        list.add("\n");
        list.add("0. Exit");
        list.add("1. Kembali ke menu utama");

        return list;
    }

    public static List<String> resultFailed() {
        List<String> list = new ArrayList<String>();
        list.add(top);
        list.add(title);
        list.add(top);
        list.add("File tidak ditemukan ");
        list.add("\n");
        list.add("0. Exit");
        list.add("1. Kembali ke menu utama");

        return list;
    }

    public static void printMenu() {
        Iterator<String> it = MainMenu.mainMenu().iterator();
        while(it.hasNext()) {
            System.out.println(it.next());
        }
    }

    public static void printResult() {
        Iterator<String> it = MainMenu.result().iterator();
        while(it.hasNext()) {
            System.out.println(it.next());
        }
    }

    public static void printMenuFailed() {
        Iterator<String> it = MainMenu.resultFailed().iterator();
        while(it.hasNext()) {
            System.out.println(it.next());
        }
    }
}
