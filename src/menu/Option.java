package menu;

import process.WriteClass;

import java.util.Scanner;

public class Option {

    Scanner input = new Scanner(System.in);
    public void option1() {
        MainMenu.printMenu();
        int pilihan = input.nextInt();

        while (pilihan !=0) {
            switch (pilihan) {
                case 1:
                    System.out.println("generate txt untuk menampilkan modus");
                    WriteClass.writeModus();
                    break;
                case 2:
                    System.out.println("generate txt untuk menampilkan rata-rata, median: ");
                    WriteClass.writeMean();
                    break;
                case 3:
                    System.out.print("masukkan nilai: ");
                    int input_group = input.nextInt();
                    WriteClass.writeGroup(input_group);
                    break;
                default:
                    System.out.println("pilihan tidak ada");
                    break;
            }
//            stop looping
            MainMenu.printMenu();
            pilihan = input.nextInt();
        }
    }
}
